<?php

namespace Tests\Feature;

use Tests\TestCase;

class RegisterTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */

    public function testRegister()
    {
        $payload = [
            'name' => 'Loan',
            'email' => 'loan.vt@neo-lab.vn',
            'password' => 'loan123',
            'password_confirmation' => 'loan123',
        ];

        $this->json('post', '/api/register', $payload)
            ->assertStatus(201)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'name',
                    'email',
                    'created_at',
                    'updated_at',
                    'api_token',
                ],
            ]);
    }
}
