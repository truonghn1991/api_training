<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Post;
use Auth;
use Gate;

class PostController extends Controller
{
    public function index()
    {
        $posts = Post::published()->paginate();

        return view('posts.index', compact('posts'));
    }

    public function create()
    {
        return view('posts.create');
    }

    public function store(StorePostRequest $request)
    {
        try {
            $posts = new Post();

            $posts->title = $request->title;
            $posts->body = $request->body;
            $posts->slug = str_slug($request->title);
            $posts->user_id = Auth::user()->id;

            $posts->save();

            return redirect()->route('edit_post', ['id' => $posts->id]);
        } catch (Exception $e) {
            \Log::error($e);
        }
    }

    public function drafts()
    {
        try {
            $postsQuery = Post::unpublished();

            if (Gate::denies('see-all-drafts')) {
                $postsQuery = $postsQuery->where('user_id', Auth::user()->id);
            }
            $posts = $postsQuery->paginate();

            return view('posts.drafts', compact('posts'));
        } catch (Exception $e) {
            \Log::error($e);
        }
    }

    public function edit(Post $post)
    {
        return view('posts.edit', compact('post'));
    }

    public function update(Post $post, UpdatePostRequest $request)
    {
        try {
            $post->title = $request->title;
            $post->body = $request->body;
            $post->slug = str_slug($request->title);

            $post->update();

            return back();
        } catch (Exception $e) {
            \Log::error($e);
        }
    }

    public function publish(Post $post)
    {
        try {
            $post->published = true;
            $post->save();

            return back();
        } catch (Exception $e) {
            \Log::error($e);
        }
    }

    public function show($id)
    {
        $post = Post::published()->findOrFail($id);

        return view('posts.show', compact('post'));
    }
}
